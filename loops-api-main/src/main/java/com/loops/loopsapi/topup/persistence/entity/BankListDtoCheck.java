package com.loops.loopsapi.topup.persistence.entity;

import lombok.Data;

/**
 * A DTO for the {@link BankList} entity
 */
@Data
public class BankListDtoCheck {
    private Long virtualAcc;
}